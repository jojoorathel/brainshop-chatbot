package com.networksystems.app.api;

import com.networksystems.app.MessageModal;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiCall {
    @GET("get")
    Call<ResposeData> sendChat(
            @Query("bid") String bid,
            @Query("key") String key,
            @Query("uid") String uid,
            @Query("msg") String msg );


}
