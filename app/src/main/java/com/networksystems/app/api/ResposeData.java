package com.networksystems.app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResposeData {
    @SerializedName("cnt") @Expose private String cnt;

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }
}
